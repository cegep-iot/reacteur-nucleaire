#include "Arduino.h"
#include "ActionBoutonPresse.h"
#include "Action.h"
#include "ClientWeb.h"
#include "Reacteur.h"

ActionBoutonPresse::ActionBoutonPresse(ClientWeb* p_clientWeb): m_clientWeb(p_clientWeb)
{
    ;
}

void ActionBoutonPresse::executer()
{
    this->m_clientWeb->changerEtat();
}
