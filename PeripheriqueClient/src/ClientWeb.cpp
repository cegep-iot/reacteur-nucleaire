#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include "config.h"
#include "ClientWeb.h"

ClientWeb::ClientWeb(Reacteur* p_reacteur, DEL* p_delVerte, DEL* p_delRouge):
                                                    m_reacteur(p_reacteur),
                                                    m_delVerte(p_delVerte),
                                                    m_delRouge(p_delRouge)
{
    this->m_dateDerniereMAJ = millis();
}

void ClientWeb::obtenirEtat()
{
    if (WiFi.status() == WL_CONNECTED)
    {
        String etat = "";
        String adresseIpServeur = SERVER_IP_ADDRESS;
        String url = "http://" + adresseIpServeur + "/coeur-reacteur";

        HTTPClient httpClient;
        httpClient.begin(url);

        int codeStatut = httpClient.GET();
    
        if (codeStatut != 200)
        {
            Serial.println(HTTPClient::errorToString(codeStatut));
        }
        else
        {
            etat = httpClient.getString();
            etat = this->deserialiserEtat(etat);

            if(etat != this->m_reacteur->obtenirEtat())
            {
                this->m_reacteur->modifierEtat(etat);
            }
        }
    }
    else
    {
        Serial.println("Non connecté au WiFi !");
    }
}

void ClientWeb::signalerEtat()
{
    const String etat = this->m_reacteur->obtenirEtat();

    if(etat == "repos")
    {
        this->m_delVerte->eteindre();
        this->m_delRouge->allumer();
    }
    else if(etat == "actif")
    {
        this->m_delRouge->eteindre();
        this->m_delVerte->allumer();
    }
    else
    {
        this->m_delRouge->eteindre();
        this->m_delVerte->eteindre();
    }
}

String ClientWeb::deserialiserEtat(String& p_etat)
{
    DynamicJsonDocument doc(64);
    DeserializationError error = deserializeJson(doc, p_etat);

    if (error)
    {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.f_str());
        return "error";
    }

    String etat = doc["etat"];

    return etat;
}

void ClientWeb::changerEtat()
{
    String nouvelEtat = "";

    if(this->m_reacteur->obtenirEtat() == "repos")
    {
        nouvelEtat = "actif";
    }
    else
    {
        nouvelEtat = "repos";
    }

    nouvelEtat = this->serialiserEtat(nouvelEtat);

    if (WiFi.status() == WL_CONNECTED)
    {
        String adresseIpServeur = SERVER_IP_ADDRESS;
        String url = "http://" + adresseIpServeur + "/coeur-reacteur";

        HTTPClient httpClient;
        httpClient.begin(url);
        httpClient.addHeader("Content-type", "application/json");

        int codeStatut = httpClient.PUT(nouvelEtat);

        if (codeStatut != 200)
        {
            Serial.println(HTTPClient::errorToString(codeStatut));
        }
    }
    else
    {
        Serial.println("Non connecté au WiFi !");
    }
}

String ClientWeb::serialiserEtat(String& p_etat)
{
    String nouvelEtat = "";
    StaticJsonDocument<64> doc;

    doc["etat"] = p_etat;
    serializeJson(doc, nouvelEtat);

    return nouvelEtat;
}

void ClientWeb::tick()
{
    long dateActuelle = millis();

    if(dateActuelle - this->m_dateDerniereMAJ > UPDATE_TIMER)
    {
        this->obtenirEtat();
        this->signalerEtat();
        this->m_dateDerniereMAJ = dateActuelle;
    }
}
