#include <Arduino.h>
#include <WiFi.h>
#include "Program.h"
#include "ClientWeb.h"
#include "Reacteur.h"
#include "DEL.h"
#include "Action.h"
#include "ActionBoutonPresse.h"
#include "Bouton.h"
#include "config.h"

Program::Program()
{
    Reacteur* reacteur = new Reacteur();
    DEL* delVerte = new DEL(25);
    DEL* delRouge = new DEL(26);
    ClientWeb* clientWeb = new ClientWeb(reacteur, delVerte, delRouge);
    Action* actionBoutonPresse = new ActionBoutonPresse(clientWeb);
    Bouton* bouton = new Bouton(16, actionBoutonPresse);

    this->m_clientWeb = clientWeb;
    this->m_bouton = bouton;

    this->connectionReseau();
}

void Program::connectionReseau()
{
    const uint8_t nbEssaisMaximum = 30;
    uint8_t nbEssais = 0;

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

    Serial.print("Connexion : ");

    while (nbEssais < nbEssaisMaximum && WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        nbEssais++;
    }

    Serial.println("");

    if (WiFi.status() == WL_CONNECTED) {
        Serial.print("Connecté au réseau WiFi, adresse IP : ");
        Serial.println(WiFi.localIP());
        Serial.println("");
    }
}

void Program::loop()
{
    this->m_clientWeb->tick();
    this->m_bouton->tick();
}
