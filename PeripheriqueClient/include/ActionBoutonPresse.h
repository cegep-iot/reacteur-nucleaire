#pragma once
#include <Arduino.h>
#include "Action.h"
#include "ClientWeb.h"
#include "Reacteur.h"

class ActionBoutonPresse : public Action {
    private:
        ClientWeb* m_clientWeb;

    public:
        ActionBoutonPresse(ClientWeb* p_clientWeb);
        virtual void executer();
};
