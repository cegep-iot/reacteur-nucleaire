#pragma once

// Temps entre chaque mise a jour de l'etat
#define UPDATE_TIMER 2000

// Informations de connections:
#define WIFI_SSID "network"
#define WIFI_PASSWORD "password"
#define SERVER_IP_ADDRESS "192.168.1.2"
