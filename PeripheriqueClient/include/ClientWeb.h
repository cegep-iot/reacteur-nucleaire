#pragma once
#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include "Reacteur.h"
#include "DEL.h"

class ClientWeb {
    private:
        Reacteur* m_reacteur;
        DEL* m_delVerte;
        DEL* m_delRouge;
        long m_dateDerniereMAJ;

        void obtenirEtat();
        String deserialiserEtat(String& p_etat);
        void signalerEtat();
        String serialiserEtat(String& p_etat);

    public:
        ClientWeb(Reacteur* p_reacteur, DEL* p_delVerte, DEL* p_delRouge);
        void changerEtat();
        void tick();
};
