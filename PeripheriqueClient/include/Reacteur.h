#pragma once
#include <Arduino.h>

class Reacteur {
    private:
        String m_etat;

    public:
        Reacteur();
        const String obtenirEtat();
        void modifierEtat(String& p_etat);
};
