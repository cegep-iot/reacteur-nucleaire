#pragma once
#include <Arduino.h>
#include "ClientWeb.h"
#include "Bouton.h"

class Program {
    private:
        ClientWeb* m_clientWeb;
        Bouton* m_bouton;

    public:
        Program();
        void loop();
        void connectionReseau();
};
