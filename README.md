# Travail Pratique: Reacteur nucléaire

Programme simulant le contrôle à distance d'un réacteur nucléaire.

## Périphérique client

Le montage comprend un ESP32, un bouton et deux DELs (une rouge, une verte). La DEL verte permet d'indiquer que le coeur du réacteur est au repos. La DEL rouge indique que le coeur du réacteur est actif. Pour connaître l'état du réacteur, vous devez utiliser une API REST. À l'appui du bouton, si le coeur du réacteur est au repos, il devient actif. Si le coeur du réacteur est actif, il devient au repos.

### Connection à votre réseau domestique ou au point d'accès

Écrivez le code qui permet de se connecter sur votre réseau domestique ou sur le point d'accès.

### Affichage du statut du coeur du réacteur

- Écrivez le code qui permet d'interroger l'API REST décrite ci-dessous afin de savoir si le réacteur est actif ou au repos:

 > Le programme interne doit exposer une application web sur le port 80. L'application web comprend une API REST qui permet de piloter le coeur du réacteur.
>
> L'API est définie comme suit :
> 
> | Ressource       | Méthode | Résultat                                              |
> |-----------------|---------|-------------------------------------------------------|
> | /coeur-reacteur | GET     | Renvoie l'état du coeur du réacteur - code 200        |
> | /coeur-reacteur | PUT     | Permet de modifier l'état du coeur et renvoie le nouvel état - code 200 |
>
> L'objet d'état doit être au format suivant :
> ```json
> {
>     "etat": "actif"
> }
> OU
> {
>     "etat": "repos"
> }
> ```

- Écrivez le code qui permet d'aller chercher cet état toute les 2000 millisecondes. Ce temps doit être facilement paramétrable. Une fois le statut récupéré, vous devez afficher le statut à l'aide de la DEL verte ou rouge.

### Prise en charge du bouton

- Écrivez le code qui permet de détecter l'appui sur le bouton. À l'appuie sur le bouton poussoir, vous devez valider le statut actuel du réacteur et ensuite envoyer le bon ordre au réacteur à l'aide d'une requête ```PUT``` :

- Si le coeur est déjà actif, envoyez une requête de mise en repos
- Si le coeur est au repos, envoyez une requête d'activation

### Validation du fonctionnement à l'aide d'une API en C#

Lancer l'application "M13_CoeurReacteurCSharp", vous pouvez soit passer par Visual Studio en ouvrant la solution ou vous pouvez lancer l'exécution dans une console en vous plaçant dans le répertoire du projet et en tapant la commande ```dotnet run```.

L'application expose un site sur le port 80 de votre ordinateur. Vous pouvez accéder à ce site en lançant votre navigateur web et en tapant l'adresse suivante : http://localhost:80. Vous pouvez tester l'application web en validant que le statut de la simulation fonctionne quand vous cliquez sur le bouton "Activer le coeur" ou "Désactiver le coeur".

![Application web du simulateur - État repos](img/ex4_simulateur_repos.png)
![Application web du simulateur - État actif](img/ex4_simulateur_actif.png)

Une fois le simulateur lancé, vous devez modifier l'adresse IP du réacteur dans le code de votre ESP32 par celle de votre ordinateur.

Si votre ESP32 est bien configuré, vous devriez voir le statut du coeur du réacteur changer à chaque pression du bouton sur votre ESP32.
